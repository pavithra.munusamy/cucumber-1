package stepDefinition;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Demologin {
	static WebDriver driver=null;
	
	@Given("Launch browser with url")
	public void launch_browser_with_url() {
		driver = new ChromeDriver();
	    driver.get("https://demowebshop.tricentis.com/");
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	        
	    }
	   // System.out.println("Browser Launch");
	

	@Then("click on login button")
	public void click_on_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	  // System.out.println("Login");
	}

	@Then("Give email")
	public void give_email() {
		driver.findElement(By.id("Email")).sendKeys("pavipragya@gmail.com");
	  // System.out.println("Email as pavipragya@gmail.com");
	}


	
	@Then("enter the password")
	public void enter_the_password() {
		driver.findElement(By.name("Password")).sendKeys("pragya@123");
	   // System.out.println("Password as pragya@123");
	}
	
	@Then("Login button")
	public void login_button() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	   
		//System.out.println("Click login button");
	}

	@Then("Click logout")
	public void click_logout() {
		driver.findElement(By.linkText("Log out")).click();
	    System.out.println("Logout");
	}


}
